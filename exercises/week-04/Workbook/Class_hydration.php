<?php

Class Name {
    public $name;
}

$names = [
    [
        'name' => 'Joe Bloggs',
    ], [
        'name' => 'Jane Doe',
    ],
];

$nameobjects = [];

foreach ($names as $name) {
    $instance = new Name();
    $instance->name = $name['name'];

    $nameobjects[] = $instance;
}

var_dump($nameobjects);
