<?php

Class User
{
    public $name = 'Joe Bloggs';
}

$user = new User();

echo $user->name . '<br>';

$user->name = 'Jane Doe';

echo $user->name . '<br>';

$user2 = new User();

echo $user2->name . '<br>';

$user2 ->name = 'Tayjon';

?>