<?php

$result='';
if(isset($_POST)) {
    $result = $_POST['a'] + $_POST['b'];
    echo $result;
    die();
}

if(isset($_GET)) {
    $result = $_GET['a'] + $_GET['b'];
    echo $result;
    die();

}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <title>Maths Fun</title>
</head>

<body>
<div class="container">
    <h1>Calculator</h1>
    <form action="" method="post" class="form p-4 m-4" id="form">
        <div class="form-inline">
            <div class="form-group">
                <label for="a">A:</label>
                <input type="number" name="a" id="a" class="form-control mt-2"><span class="mt-2">+</span>
            </div>
            <div class="form-group">
                <label for="b">B:</label>
                <input type="number" name="b" id="b" class="form-control mt-2"><span class="mt-2">=</span>
            </div>
            <div class="form-group">
                <label for="Result">Result:</label>
                <input type="text" name="result" id="result" class="form-control mt-2" readonly value="<?=$result?>"
            </div>
        </div>
        <div class="form-inline">
            <button type="submit" class="btn btn-primary">Calculate</button>
        </div>
    </form>
</div>
</body>
<script>
    window.addEventListener('DOMContentLoaded', function() {
        let form = document.getElementById('form');
        form.addEventListener('submit', function (e) {
            e.preventDefault();
            calculate();
        });
    });

    function calculate() {
        let a = document.getElementById('a');
        let b = document.getElementById('b');
        let result = document.getElementById('result');

        axios.get('Get parameters.php?a=' + a.value + '&b=' + b.value)
            .then(function (response) {
                console.log(response.data);
                result.value = response.data
            })
            .catch(function (error) {
                console.log(error);
            });


    }

</script>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js" integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF" crossorigin="anonymous"></script>
<!--Axios -->
<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>


</html>