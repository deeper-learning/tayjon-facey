axios
.get('http://localhost:8080/checkins')
.then(resp=> {
    //success
    console.log(resp);
    if (resp.data.length> 0){
        $('#rating').text(resp.data[0].rating);
        const name=$('<h3></h3>').text(resp.data[0].name)
        const rating=$('<span></span>').text(resp.data[0].rating)
        const dateTime=$('<p></p>').text(resp.data[0].dateTime)
        const review=$('<p></p>').text(resp.data[0].review)
        $('#review').append(name)
        $('#review').append(rating)
        $('#review').append(dateTime)
        $('#review').append(review)
    }

})
    .catch(err => {
        //error
        console.log(err);
})