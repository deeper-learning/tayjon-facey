<?php

class Review
{
    public $review;
}

if(!empty($_POST)) {
    $Review = new Review();
    $Review->review = $_POST['review'];

    $databaseUserUsername = 'root';
    $databaseUserPassword = 'root';

    try {
        $dbh = new PDO('mysql:host=mysql;dbname=lecture',

        $databaseUserUsername,
        $databaseUserPassword,
    );

    } catch (PDOException $e) {
        var_dump($e);

        die('Unable to establish a database connection');
    }
}


?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Add New Review</title>
</head>
<body>
<h2>Enter new Review</h2>
<?php if (isset($message)): ?>
     New Review; <?= $message ?>
<?php endif; ?>
    <form action="" method="post">
        <label for="review">Enter Review:</label>
        <input type="text" name="review" id="review" class="form-control form-control-lg">

        <button type="submit">Save Review</button>
    </form>
</body>
</html>
