<?php

require_once 'db.php';

$productId = 4;
$productTitle = 'Updated product title';

$stmt = $dbh->prepare(
    'UPDATE product set title = :title WHERE id = :id'
);

$stmt->execute([
    'id' => $productId,
    'title' =>$productTitle
]);

echo '# Rows Affected:' . $stmt->rowCount();
