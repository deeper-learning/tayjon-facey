<?php

Use App\Entity\Product;

require_once '../src/setup.php';

if (!empty($_POST['title']) && !empty($_POST['description'])) {
    $formData = [
        'title' => strip_tags($_POST['description']),
        'description' => strip_tags($_POST['description']),
        ];

    $formProduct = new Product();
    $formProduct->title = $formData['title'];
    $formProduct->description = $formData['description'];


    $product = $dbProvider->createProduct($formProduct);
    $logger->info('product created: ' . $product->title);
    header( 'Location: product.php?productId=' . $product->id);
    exit;
}

?>
<!Doctype html>
<html lang="en">
<head>
title>Bootstrap Example</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">

<style>
    @media screen and (max-width: 700px) {
        div.column,
        max width: 100%;
        display: inline-block;

        .star-rating {
            background-color: grey;
            width: 200px;
            height: 30px;
            display: inline-block;
        }

        .star-rating div {
            height: 100%;
            background-color: cornflowerblue;

        }

        h1 {
            color: rgb(0, 0, 0);
            font-family: 'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif;
            font-size: 200%;

        }
        p {
            color: rgb(0, 0, 0);
            font-family: 'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif;
            font-size: 100%;
        }

        </style>
</head>
<body>
<form>
<h1>Create Product</h1>
<div>
<label for="Add new product">Add New Product</label>
<input type="text" name="product" id="product">
</div>

<button type="button" class="btn btn-primary">Add Product</button>

</form>
</body>
</html>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               </form>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    </body>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      </html>