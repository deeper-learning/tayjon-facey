<?php

require_once 'db.php';

$productAddedSuccess = false;

if (!empty($_POST)) {
    $newProductTitle = $_POST['title'];

    $stmt = $dbh->prepare(
        'INSERT INTO product (title) VALUES (:title);'
    );

    $stmt->execute([
        'title' => $newProductTitle
    ]);

    if ($stmt->rowCount() > 0){
    $productAddedSuccess = true;
    }
}


?>
<!DOCTYPE html>
<html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Product Form</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <style>

@media screen and (max-width: 700px) {
    div.column,
            max width: 100%;
            display: inline-block;
        }

        h1 {
    color: rgb(0, 0, 0);
    font-family: 'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif;
            font-size: 200%;

        }
        p {
    color: rgb(0, 0, 0);
    font-family: 'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif;
            font-size: 100%;
        }

    </style>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
</head>
<body class="container pt-4">
    <form action="" method="post">
        <?php if ($productAddedSuccess): ?>
        <p class="alert alert-success">Product added successfully!</p>
        <?php endif; ?>
        <div class="form-group">
        <label for="product-title">Title:</label>
            <input type="text" name="title" id="product-title" class="form-control">
        </div>

        <button type="button" class="btn btn-primary">Save Product</button>


</form>
</body>
    </html>