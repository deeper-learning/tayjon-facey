<?php

$username = 'root';
$password = 'root';

try {
    $dbh = new PDO(
        'mysql:dbname=MyProject;host=mysql',
        $username,
        $password
    );

    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {

    die('Unable to establish a database connection');

}
