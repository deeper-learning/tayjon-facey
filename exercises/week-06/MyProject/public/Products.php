<?php


class Product
{
    public int $id;
    public string $title;
    /** @var CheckIn[] */
    public array $checkins;
}