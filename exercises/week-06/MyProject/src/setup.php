<?php

require_once '../../vendor/autoload.php';

use App\DatabaseProvider;


$whoops = new \Whoops\Run();
$whoops->pushHandler(
    new \Whoops\Handler\PrettyPageHandler()
);
$whoops->register();

$dotenv = \Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();

$logger = new \Monolog\logger('application');
$logger->pushHandler(
    new \Monolog\Handler\StreamHandler(
        'application.log',
        \Monolog\Logger::WARNING
    )
);

$dbProvider = new \App\DataProvider\DatabaseProvider();

try {
    $dbh = new PDO(
        'mysql:dbname=lecture;host=mysql',
        $username,
        $password
    );

    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {

    die('Unable to establish a database connection');

}
