<?php


namespace App\Hydrator;

use App\Entity\CheckIn;
use App\Entity\Product;


class EntityHydrator
{
    public function hydrateProduct(): Product
    {
//...
    }

    public function hydrateCheckIn(): CheckIn
    {
//...
    }

    public function hydrateProductWithCheckIns(array $data): Product
    {

        $product = new Product();
        $product->id = $data[0]['product_id'];
        $product->title = $data[0]['title'];

        foreach ($data as $checkinRow) {
            $checkIn = new CheckIn();
            $checkIn->id = $checkinRow['id'];
            $checkIn->review = $checkinRow['review'];
            $checkIn->productId = $checkinRow['product_id'];

            $product->addCheckin($checkIn);
        }

        return $product;
    }
}

